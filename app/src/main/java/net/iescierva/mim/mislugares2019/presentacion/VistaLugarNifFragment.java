/*
 * Copyright (c) 2020. Miguel Angel
 */

package net.iescierva.mim.mislugares2019.presentacion;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import net.iescierva.mim.mislugares2019.Aplicacion;
import net.iescierva.mim.mislugares2019.R;
import net.iescierva.mim.mislugares2019.casos_uso.CasosUsoLugarFecha;
import net.iescierva.mim.mislugares2019.datos.AdaptadorLugaresBD;
import net.iescierva.mim.mislugares2019.datos.LugaresBD;
import net.iescierva.mim.mislugares2019.modelo.Lugar;

public class VistaLugarNifFragment extends Fragment {


    private AdaptadorLugaresBD adaptador;
    private LugaresBD lugares;
    private int pos;
    private Lugar lugar;
    private View v;

    @Override
    public View onCreateView(LayoutInflater inflador,
                             ViewGroup contenedor,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View vista = inflador.inflate(R.layout.elemento_lista_nif, contenedor, false);
        v = vista;
        return vista;
    }

    //recogida de parámetros e inicialización
    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null)
            pos = extras.getInt("pos", 0);
        else
            pos = 0;
        initialize();
    }

    private void initialize() {
        adaptador = ((Aplicacion) getActivity().getApplication()).getAdaptador();
        lugares = ((Aplicacion) getActivity().getApplication()).getLugares();
        lugar = adaptador.lugarPosicion(pos);
        mostrarDatos();
    }

    private void mostrarDatos() {
        TextView nombre = v.findViewById(R.id.nombre);
        TextView nif = v.findViewById(R.id.nif);

        nombre.setText(lugar.getNombre());
        nif.setText(lugar.getNif());

    }


}
