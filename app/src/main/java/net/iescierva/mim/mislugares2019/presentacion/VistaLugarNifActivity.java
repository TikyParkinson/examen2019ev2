/*
 * Copyright (c) 2020. Miguel Angel
 */

package net.iescierva.mim.mislugares2019.presentacion;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import net.iescierva.mim.mislugares2019.R;

public class VistaLugarNifActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vista_lugar_nif);
    }
}
